# Name: Oscar Liang
# zID: z5363102

def password_evaluate(password_input):
    if password_input == "":
        return "Your password strength will appear here after you have analysed your password."
    elif len(password_input) > 8 and password_input != password_input.lower() and password_input != password_input.upper():
        return "Your password strength is strong."
    elif len(password_input) > 8 or (password_input != password_input.lower() and password_input != password_input.upper()):
        return "Your password strength is weak."
    else:
        return "Your password strength is very weak."