# Name: Oscar Liang
# zID: z5363102

from flask import Flask, request, session
from pyhtml import html, head, body, h2, p, br, link, input_, label, form, table, tr, th, td, button, div
from secrets import token_hex

default_restaurants = {
    "Billy's Burgers": {
        "price_range": "$",
        "options": []
    },
    "Harbour Seafood": {
        "price_range": "$$$",
        "options": ["gluten-free"]
    },
    "UNSW Restaurant": {
        "price_range": "$$",
        "options": ["gluten-free", "vegetarian"]
    },
    "It's a Thai": {
        "price_range": "$",
        "options": ["vegetarian"]
    }
}

default_filters = {
    "$" : True,
    "$$" : True,
    "$$$" : True,
    "gluten-free" : False,
    "vegetarian" : False,
    "halal" : False,
}

def add_restaurant():
    options = []
    if "restaurant_gluten_free" in request.form:
        options = options + ["gluten-free"]
    if "restaurant_vegetarian" in request.form:
        options = options + ["vegetarian"]
    if "restaurant_halal" in request.form:
        options = options + ["halal"]
    session["restaurant_list"][request.form["restaurant_name"]] = {
        "price_range": request.form["restaurant_price_range"],
        "options": options
    }
    session.modified = True

def remove_restaurant():
    print(request.form["remove_restaurant"][7:])
    session["restaurant_list"].pop(request.form["remove_restaurant"][7:])
    session.modified = True

def update_filters():
    if "filter_$" in request.form:
        session["restaurant_filters"]["$"] = True
    else: 
        session["restaurant_filters"]["$"] = False
    if "filter_$$" in request.form:
        session["restaurant_filters"]["$$"] = True
    else: 
        session["restaurant_filters"]["$$"] = False
    if "filter_$$$" in request.form:
        session["restaurant_filters"]["$$$"] = True
    else: 
        session["restaurant_filters"]["$$$"] = False
    if "filter_gluten_free" in request.form:
        session["restaurant_filters"]["gluten-free"] = True
    else: 
        session["restaurant_filters"]["gluten-free"] = False
    if "filter_vegetarian" in request.form:
        session["restaurant_filters"]["vegetarian"] = True
    else: 
        session["restaurant_filters"]["vegetarian"] = False
    if "filter_halal" in request.form:
        session["restaurant_filters"]["halal"] = True
    else:
        session["restaurant_filters"]["halal"] = False
    print(session["restaurant_filters"])

def get_table_data(restaurant, price_range, options_data, filter_list):
    if "(" + price_range + ")" not in filter_list:
        return []
    elif "gluten-free" in filter_list and "Gluten Free" not in options_data:
        return []
    elif "vegetarian" in filter_list and "Vegetarian" not in options_data:
        return []
    elif "halal" in filter_list and "Halal" not in options_data:
        return []
    else: 
        return [html(
            tr(
                td(restaurant),
                td(price_range),
                td(options_data),
                # td(str(session["restaurant_list"][restaurant]["options"]).replace("[", "").replace("]", "").replace("'", "").replace("-f", " F")),
            ),
        )]

def get_filter_data(id):
    if id == 1:
        if session["restaurant_filters"]["$"]:
            return input_(type="checkbox", checked = True, name="filter_$", id="filter_$"),
        else:
            return input_(type="checkbox", name="filter_$", id="filter_$")
    if id == 2:
        if session["restaurant_filters"]["$$"]:
            return input_(type="checkbox", checked = True, name="filter_$$", id="filter_$$"),
        else:
            return input_(type="checkbox", name="filter_$$", id="filter_$$")
    if id == 3:
        if session["restaurant_filters"]["$$$"]:
            return input_(type="checkbox", checked = True, name="filter_$$$", id="filter_$$$"),
        else:
            return input_(type="checkbox", name="filter_$$$", id="filter_$$$")
    if id == 4:
        if session["restaurant_filters"]["gluten-free"]:
            print(session["restaurant_filters"]["$"])
            return input_(type="checkbox", checked = True, name="filter_gluten_free", id="filter_gluten_free"),
        else:
            return input_(type="checkbox", name="filter_gluten_free", id="filter_gluten_free")
    if id == 5:
        if session["restaurant_filters"]["vegetarian"]:
            return input_(type="checkbox", checked = True, name="filter_vegetarian", id="filter_vegetarian"),
        else:
            return input_(type="checkbox", name="filter_vegetarian", id="filter_vegetarian")
    if id == 6:
        if session["restaurant_filters"]["halal"]:
            return input_(type="checkbox", checked = True, name="filter_halal", id="filter_halal"),
        else:
            return input_(type="checkbox", name="filter_halal", id="filter_halal")

def get_restaurant_html():
    table_data = [
        tr(
            th("Restaurant Name"),
            th("Price Range"),
            th("Options"),
        )
    ]
    remove_data = []
    for restaurant in session["restaurant_list"]:
        if "price_range" in session["restaurant_list"][restaurant]:
            options_data = ""
            if session["restaurant_list"][restaurant]["options"] == []:
                options_data = "None"
            else:
                options_data = ", ".join(session["restaurant_list"][restaurant]["options"]).title().replace("-", " ")
            filter_list = ""
            for filter in session["restaurant_filters"]:
                if session["restaurant_filters"][filter] == True:
                    filter_list = filter_list + "(" + filter + ")"
            print(filter_list)
            table_data = table_data + get_table_data(restaurant, session["restaurant_list"][restaurant]["price_range"], options_data, filter_list)
            remove_data = remove_data + [html(
                form(
                    input_(type="submit", name = "remove_restaurant", value = "Remove " + str(restaurant))
                )
            )]

    return html(
        head(
            link(rel="stylesheet", href="static/style.css")
        ),
        body(
            h2("Welcome to the Restaurant Finder!"),
            form(
                "Price range options: ",
                get_filter_data(1),  
                label(for_="filter_$")("$"),
                get_filter_data(2),
                label(for_="filter_$$")("$$"),
                get_filter_data(3),
                label(for_="filter_$$$")("$$$"),
                br(),
                "Diet options: ",
                get_filter_data(4),
                label(for_="filter_gluten_free")("Gluten Free"),
                get_filter_data(5),
                label(for_="filter_vegetarian")("Vegetarian"),
                get_filter_data(6),
                label(for_="filter_halal")("Halal"),
                br(),
                input_(type="submit", value="Update" , name = "filter_update"),
                input_(type="submit", value="Reset", name = "filter_reset"),
            ),
            p("List of restaurants according to your filters"),
            table(table_data),
            br(),
            button(onClick = "document.getElementById('restaurant_overlay').style.display='block';")("Add Restaurant"),
            button(onClick = "document.getElementById('remove_overlay').style.display='block';")("Remove Restaurant"),
            form(action="..")(
                input_(type="submit", value="Go back")
            ),
            div(id = "restaurant_overlay", class_ = "overlay")(
                div(id = "restaurant_tab", class_ = "tab")(
                    div(id = "restaurant_container", class_ = "container")(
                        h2("Add Restaurant"),
                        div (id = "restaurant_form_div")(form(
                            "Restaurant name: ",
                            input_(type="text", name="restaurant_name", id="restaurant_name", placeholder="Enter restaurant name here", required = "True"),
                            br(),
                            "Price range options: ",
                            input_(type="radio", name="restaurant_price_range", id="restaurant_price_range_1", value="$", required = "True"),
                            label(for_="restaurant_price_range_1")("$"),
                            input_(type="radio", name="restaurant_price_range", id="restaurant_price_range_2", value="$$", required = "True"),
                            label(for_="restaurant_price_range_2")("$$"),
                            input_(type="radio", name="restaurant_price_range", id="restaurant_price_range_3", value="$$$", required = "True"),
                            label(for_="restaurant_price_range_3")("$$$"),
                            br(),
                            ("Diet options: "),
                            input_(type="checkbox", name="restaurant_gluten_free", id="restaurant_gluten_free", value="Gluten Free"),
                            label(for_="restaurant_gluten_free")("Gluten Free"),
                            input_(type="checkbox", name="restaurant_vegetarian", id="restaurant_vegetarian", value="Vegetarian"),
                            label(for_="restaurant_vegetarian")("Vegetarian"),
                            input_(type="checkbox", name="restaurant_halal", id="restaurant_halal", value="Halal"),
                            label(for_="restaurant_halal")("Halal"),
                            br(),
                            br(),
                            input_(type = "submit", value ="Add"),
                            input_(type = "button", onClick = "document.getElementById('restaurant_overlay').style.display='none';", value = "Cancel"),
                        )), 
                    ),
                ),
            ),
            div(id = "remove_overlay", class_ = "overlay")(
                div(id = "remove_tab", class_ = "tab")(
                    div(id = "remove_container", class_ = "container")(
                        h2("Remove Restaurant"),
                        div(id="remove_div")(remove_data),
                        form(
                            input_(type="submit", value="Cancel"),
                        ),
                    ),
                ),
            ),
        ),
    )