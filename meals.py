# Name: Oscar Liang
# zID: z5363102

# Written by Sim Mautner on 10/7/22 for the purpose of COMP1010 labwork.
import requests
from flask import Flask, request
from pyhtml import html, head, link, body, select, option, form, input_, li, ul, h1, p, label

app = Flask(__name__)

def get_categories():
    return [] # TODO make this your function

def get_recipe_names_by_category(category):
    return [] # TODO make this your function

def list_to_dropdown_pyhtml(list_name, my_list):
    pyhtml_list = []
    for item in my_list:
        pyhtml_list.append(option(item))
    return select(name=list_name)(pyhtml_list)

def list_to_pyhtml(my_list):
    
    items_in_pyhtml = []
    for item in my_list:
        items_in_pyhtml.append(li(item))
    
    return ul(items_in_pyhtml)

meals_html = html(
    head(
        link(rel="stylesheet", href="static/style.css")
    )
)