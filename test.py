from flask import Flask, request, session
from pyhtml import html, head, link, body, p, form, input_, span, ul, li
from json import dumps
from random import randint
from secrets import token_hex

APP = Flask(__name__)
APP.config['SECRET_KEY'] = token_hex(32)

@APP.route('/', methods=['GET'])
def home():
    session['prev_num'] = randint(0, 3)
    return str(html(
        head(
            link(rel='stylesheet', href='static/test_style.css')
        ),
        body(
            p(session['prev_num']),
            form(action='/game')(
                input_(placeholder='username', name='username'),
                input_(type="submit", value="Start Game")
            ),
            form(action='/score')(
                input_(type='submit', value='View Score', name='score'),
            )
        )
    ))

@APP.route('/game', methods=['GET', 'POST'])
def game():
    curr_num = randint(0, 3)
    if request.method == 'POST':
        if 'same' in request.form:
            if curr_num == session['prev_num']:
                session[session['curr_user']]['correct'] += 1
            session[session['curr_user']]['total'] += 1
        elif 'not_same' in request.form:
            if curr_num != session['prev_num']:
                session[session['curr_user']]['correct'] += 1
            session[session['curr_user']]['total'] += 1
        elif 'username' in request.form:
            session['curr_user'] = request.form['username']
            if session['curr_user'] not in session:
                session[session['curr_user']] = {
                    'correct': 0,
                    'total': 0,
                }
        session['prev_num'] = curr_num
    return str(html(
        body(
            p(curr_num),
            form(
                input_(type='submit', value='Same', name='same'),
                input_(type='submit', value='Not Same', name='not_same'),
            ),
            form(action='/score')(
                input_(type='submit', value='View Score', name='score'),
            ),
            span(session[session['curr_user']]['correct']),
            span('/'),
            span(session[session['curr_user']]['total'])
        )
    ))

@APP.route('/score', methods=['GET', 'POST'])
def score():
    session_dict = dict(session)
    if 'curr_user' in session_dict:
        session_dict.pop('curr_user')
    if 'prev_num' in session_dict:
        session_dict.pop('prev_num')
    if request.method == 'POST':
        if 'score' in request.form:
            data = open('users.json', 'w')
            data.write(dumps(session_dict, indent=2))
    lis = []
    for user in session_dict:
        lis.append(li(session[user]))
    print(str(dict(session)))
    return str(html(
        body(
	        ul(
                *lis
            )
        )
    ))
if __name__ == '__main__':
    APP.run(debug=True)