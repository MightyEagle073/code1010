# Name: Oscar Liang
# zID: z5363102

from flask import Flask, request, session
from pyhtml import html, p, pre, input_, br, form, head, link, body  # TODO add imports
from string import punctuation
from password_evaluator import password_evaluate
from restaurant_finder import default_restaurants, default_filters, get_restaurant_html, add_restaurant, remove_restaurant, update_filters
from meals import get_categories, get_recipe_names_by_category, list_to_dropdown_pyhtml, list_to_pyhtml, meals_html
from json import dumps
from secrets import token_hex

app = Flask(__name__)
# app.config['SECRET_KEY'] = token_hex(32)
app.config['SECRET_KEY'] = "2020 BMW 2 Series 218i M Sport F44 Auto"

@app.route("/", methods=["GET", "POST"])
def homepage():
    if request.method == "POST":
        print("Request method returned POST")
    else:
        print("Request method returned GET")
    response = html(
        head(
            link(rel="stylesheet", href="static/style.css")
        ),
        p("Hello, which function would you like to access today?"),
        form(action="/text_analysis",)(
            input_(type="submit", value="Text Analysis", name="start_text_analysis"),
        ),
        br(),
        form(action="/password_analysis")(
            input_(type="submit", value="Password Analysis")
        ),
        br(),
        form(action="/restaurant_finder")(
            input_(type="submit", value="Restaurant Finder")
        ),
        br(),
        form(action="/meals")(
            input_(type="submit", value="Meals")
        ),
        br(),
    )
    return str(response)

@app.route("/text_analysis", methods=["GET", "POST"])
def text_analysis():
    analysis = "Analysis will appear here after the analyse button has been pressed"
    if request.method == "POST":
        if 'text_analyse' in request.form:
            text_input = request.form["text_input"]
            analysis = "Number of characters: ", len(text_input), "\r\nNumber of alphanumeric characters: ", len(''.join(filter(str.isalnum, text_input))), "\r\nNumber of words: ", len(text_input.split(" "))
            print("Reloaded with updated analysis")
        else:
            print("Request method returned POST")
    else:
        print("Request method returned GET")
    response = html(
        head(
            link(rel="stylesheet", href="static/style.css")
        ),
        body(
            p("Please enter your text"),
            form(
                input_(name="text_input", type="text", placeholder="Enter text here", id="text_input"),
                br(),
                br(),
                input_(name="text_analyse", type="submit", value="Analyse"),
            ),
            pre(analysis),
            form(action="..")(
                input_(type="submit", value="Go back")
            )
        )
    )
    return str(response)

@app.route("/password_analysis", methods=["GET", "POST"])
def password_analysis():
    analysis = "Analysis will appear here after the analyse button has been pressed"
    if request.method == "POST":
        if "password_input" in request.form:
            password_input = request.form["password_input"]
            analysis = password_evaluate(password_input)
            print("Updated Analysis")
        else:
            print("Request method returned GET")
    else:
        print("Request method returned GET")
    response = html(
        head(
            link(rel="stylesheet", href="static/style.css")
        ),
        p("Please enter your text"),
        form(
            input_(name="password_input", type="text", placeholder="Enter text here", id="password_input"),
            br(),
            br(),
            input_(name="password_analyse", type="submit", value="Analyse"),
        ),
        pre(analysis),
        form(action="..")(
            input_(type="submit", value="Go back")
        ),
    )
    return str(response)

@app.route("/restaurant_finder", methods=["GET", "POST"])
def restaurant_finder():
    if "restaurant_list" not in session:
        session["restaurant_list"] = default_restaurants
        print("Restaurant list not detected, generated new one")
    if "restaurant_filters" not in session:
        session["restaurant_filters"] = default_filters
        print("Restaurant filters not detected, generated new one")
    if request.method == "POST":
        if "restaurant_name" in request.form and "restaurant_price_range" in request.form:
            add_restaurant()
        if "remove_restaurant" in request.form:
            remove_restaurant()
        if "filter_update" in request.form:
            update_filters()
        if "filter_reset" in request.form:
            session["restaurant_filters"] = default_filters
        print("Request method returned POST")
    else:
        print("Request method returned GET")
    return str(html(get_restaurant_html()))

@app.route("/meals", methods=["GET", "POST"])
def meals():
    if request.method == "POST":
        print("Request method returned POST")
    else:
        print("Request method returned GET")
    return str(html(meals_html))

if __name__ == "__main__":
    app.run(debug=True)